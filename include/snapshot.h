/*

The MIT License (MIT)

Copyright (c) 2013 ICVT Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#ifndef SNAPSHOT_H
#define SNAPSHOT_H

#include <stdlib.h>
#include <map>
#include <set>
#include <string>
#include <memory>
#include "allocator.impl"
#include "memmgr.h"

/*
    A snapshot is a list of memory allocations (pointers), and their data at the time
    the snapshot was taken.
 
    On restore, the memory is copied from the snapshot cloned buffers back into the original buffers
*/
class Snapshot {
public: // types /typedefs
	struct Clone {
		typedef std::shared_ptr<void> Ptr_t;
		Clone(size_t size, Ptr_t ptr, MemoryMgr::AllocIterator_t owner) : _ptr(ptr), _size(size), _owner(owner){}
		Clone(Clone const& other) : _ptr(other._ptr), _size(other._size), _owner(other._owner) {}
		Ptr_t	_ptr;
		size_t	_size;
		MemoryMgr::AllocIterator_t _owner;
	};
private: // types / typedefs:

	typedef Allocator<std::pair<void* const, Clone> > CloneAllocator_t;
	typedef std::map<void*, Clone, std::less<void*>, CloneAllocator_t> Clones_t;
	typedef Clones_t::iterator snapshot_clones_iter_t;

public: // types /typdefs
	typedef Clones_t::const_iterator snapshot_clones_citer_t;

public:
	Snapshot();
	~Snapshot();

	void addBuffer(void* ptr, Clone const&);			//add buffer to snapshot
	bool hadBuffer(void* ptr, snapshot_clones_citer_t* it = nullptr) const;					//is ptr in snapshot
	void removeBuffer(void *ptr);
	void restoreBuffer(snapshot_clones_citer_t& it) const;			//restore buffer value from snapshot
	bool updateBuffer(void* ptr, size_t size, Clones_t::iterator& it	);		//update data in clone, if src data has changed
	
	friend bool operator==(const Snapshot &lhs, const Snapshot &rhs);


private: // methods:

	void removeBuffer(snapshot_clones_iter_t it);

private: // members:

	friend class MemoryMgr;
	int _id;
	Clones_t _clones;


public: //testing methods:

	//get the buffer data's crc
	uint16_t get_checksum(void*) const;
	void dump(const char* filename) const;
	bool isDirty(void* ptr) const;
	size_t size() const {
		return _clones.size();
	}

	size_t _updateStatsNotCopied;
	size_t _updateStatsCopied;
	size_t _updateStatsResizedAndCopied;
};


#endif //SNAPSHOT_H

