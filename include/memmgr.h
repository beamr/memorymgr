/*

The MIT License (MIT)

Copyright (c) 2013 ICVT Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#ifndef MEM_MGR_H
#define MEM_MGR_H

#include <stdlib.h>
#include <memory>
#include <map>
#include <set>
#include <vector>
#include "allocator.impl"

class Snapshot;

class PtrData {
public: // methods:
	PtrData(): _id(0), _size(0), _isLive(false), _protect(false) {
	}

	PtrData(unsigned long id_, size_t size, bool protect = false) : _id(id_), _size(size), _isLive(true), _protect(protect) {
	}


public: // types / typdefs:

	typedef Allocator<Snapshot*> snapshot_groups_allocator_t;
	typedef std::set<Snapshot*, std::less<Snapshot*>, snapshot_groups_allocator_t>  AttachedSnapshots_t;
	typedef AttachedSnapshots_t::iterator snapshot_groups_iter_t;
	typedef AttachedSnapshots_t::const_iterator snapshot_groups_citer_t;

public: // members:
	unsigned long _id; //allocation identifier, assigned in order of allocation
	size_t _size;	//allocated size
	bool _isLive;	//false if pointer has been deleted by user
					//however, the pointer cannot be released since we might need to
					//restore it
	bool _protect;
	AttachedSnapshots_t	_snapshots; //set of snapshots this malloc belongs to
};


/*
    The memory manager handles all memory allocation tracking and snapshots.
    The class is a singleton and should be used via the instance method
 
    the onXXX methods are called by the hooks when memory is allocated.
    These methods keep track of the memory allocations made, and their sizes.
 
    takeSnapshot and restoreSnapshot are used to clone/restore all buffers
*/
class MemoryMgr {

public:
	static MemoryMgr& instance();

	//Handle memory allocation functions by tracking the ptr and allocated size
	void onMalloc(void* ptr, size_t size);
	void onCalloc(void* ptr, size_t nmemb, size_t size);
	void onMemAlign(void* ptr, size_t alignment, size_t size, bool protect);
	void onPosixMemAlign(void* ptr, size_t alignment, size_t size);
	void OnSegFault(void* addr, int serious);

	//Mark the allocated ptr as free'd by the user
	//If the ptr is not in any snapshot, return True which will release it
	//If the ptr is used by at least one snapshot, return false so it doesn't get released
	bool onFree(void* ptr);

	//Get size of previously allocated buffer referenced by ptr
	size_t getMallocSize(void* ptr) const;

	//take a snapshot of all live tracked allocations
	//the index parameter is the snapshot identifier (any int is okay)
	void takeSnapshot(int index);

	//restore all memory buffers to previous values, and saved when
	//snapshot was taken
	void restoreSnapshot(int index);

	//remove remove snapshot all memory items unique to this snapshot
	void releaseSnapshot(int index);

	//remove all snapshots
	void releaseAllSnapshots();

	// protect
	void protectAllMemory();

	// prints status and resets protection
	void resetAndReport();

	//called when the snapshot is deleted
	//This method updates the ptr structure, and removes the reference to the snapshot
	void removeFromSnapshot(void* ptr, const Snapshot* p);


public:	// Testing methods
	void clear();

	inline bool isTracked(void *ptr) const {
		return _allocs.find(ptr)  != _allocs.end();
	}

	// returns number of allocations
	inline unsigned int allocsCount() const {
		return _allocs.size();
	}

	// returns number of snapshots
	inline unsigned int snapshotCount() const {
		return _snapshots.size();
	}

	// dump all list of allocations and snapshots to textfile
	void dump(const char* filename) const;

	// dump all list of allocations and snapshots to textfile
	void dump(const char* filename, int index) const;

	// dump data of all allocations of a given size
	void dumpData(const char* filename, size_t minSize=0) const;

	// find allocation-id based from pointer value
	unsigned int getAllocId(void* ptr) const;

	bool isAlloced(void *ptr) const;
	bool isLive(void *ptr) const;
	int countSnapshots(void *ptr) const;

private: // types / typedefs

	typedef Allocator<std::pair<void* const, PtrData> > ptrdata_malloc_allocator_t;
	typedef std::map<void*, PtrData, std::less<void*>, ptrdata_malloc_allocator_t> Allocs_t;
	typedef Allocs_t::iterator malloc_cont_iter_t;
	typedef Allocs_t::const_iterator malloc_cont_citer_t;

	typedef Allocator<std::pair<int const, Snapshot> > snapshot_malloc_allocator_t;
	typedef std::map<int, Snapshot, std::less<int>, snapshot_malloc_allocator_t> Snapshots_t;
	typedef Snapshots_t::iterator snapshots_cont_iter_t;
	typedef Snapshots_t::const_iterator snapshots_cont_citer_t;

public: // for PtrData
	typedef Allocs_t::iterator AllocIterator_t;

private: // methods:
	MemoryMgr();
	~MemoryMgr();

	void AddPtr(void* ptr, size_t size, bool protect = false);

	void CopyPtrToSnapshots(Allocs_t::iterator& data);
	void AddClone(Snapshot& to, AllocIterator_t& it);
private:

	Allocs_t 		_allocs; //collection of all memory allocations
	Snapshots_t 	_snapshots; //collection of all snapshots.
	bool 			_safe;	//disable safety checkes
	unsigned long 	_counter; //allocation counter
	std::shared_ptr<void> _dummy_memory;

	friend class UpdateSnapshot;

};

typedef std::pair<void*, MemoryMgr::AllocIterator_t> vdata_t;
typedef Allocator<vdata_t> vdata_allocator_t;
typedef std::vector<vdata_t, vdata_allocator_t> vdata_cont_t;
typedef vdata_cont_t::iterator vdata_cont_iter_t;
#endif //MEM_MGR_H
