#ifndef _MMGR_H
#define _MMGR_H

#include <stddef.h>

extern "C" {

void *mmgr_malloc(size_t size);
void mmgr_free(void* ptr);

}

#endif
