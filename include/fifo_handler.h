/*

The MIT License (MIT)

Copyright (c) 2013 ICVT Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#ifndef FIFO_HANDLER_H
#define FIFO_HANDLER_H

#include <iostream>
#include <fstream>
#include <string>
#include <mutex>
#include <cassert>
#include <thread>
#include <sys/stat.h>
#include "logger.h"

/* 
   FIFOOpener is a helper class that spawns a new thread to open the fifo
   We use this class, since openning a FIFO will block until the other side of the fifo is openned
 
   Since we have mulitple fifos, and the order on the other side is not know, we open each one in
   a thread, and then join on all threads.
*/
template<typename T>
class FIFOOpener {
public:
	FIFOOpener(T& f, const std::string& name, std::ios::openmode mode) :
	_mode(mode), _name(name), _f(f) {
		logger() << "FIFOOpener - " << _name << std::endl;
	}

	void operator()() {
		int attempts = 3;

		while(attempts > 0) {
			logger() << "Going to open fifo " << _name << std::endl;
			_f.open(_name, _mode);

			if(_f.is_open()) {
				logger() << "fifo is open" << std::endl;
				break;
			} else {
				--attempts;
				sleep(1);
			}
		}
		if(!_f.is_open()) { 
			std::cerr << "couldn't open fifo " << _name << std::endl;
			exit(-1);
		}
	}

private:
	std::ios::openmode _mode;
	const std::string& _name;
	T& _f;
};


/*
	FIFOHandler

		Creates two fifos for receiving memory_mgr commands and sending results
		The FIFOs are created using mkfifo in /tmp (set environment variable MMGR_FIFO_PATH to override)
	- /tmp/memmgr.pid.in - external process should write commands to this fifo (don't forget to fflush)
    - /tmp/memmgr.pid.out - external process should read status from this fifo
 
    The external process must send an "exit" command, or close the fifos, otherwise the FIFOHandler will
    block inside the DTor
*/
class FIFOHandler {
private:
	FIFOHandler();
	~FIFOHandler();					//wait for all threads to finish, and remove fifos

public:
	void init();					//initialize the fifos, 
	static FIFOHandler& instance();

	void start();					//spawn new thread to handle the fifos
	void operator()();				//called by the new thread spawned in the start method
									//read from the input fifo, send commands to the memory mgr, and 
									//write response to output fifo

private:
	std::thread* _reader_thread;	//A thread reading commands from the input fifo (typically blocked on read)
	std::string _reader_name;
	std::string _writer_name;
	std::ifstream _reader;			//the input fifo to receive commands from
	std::ofstream _writer;			//the output fifo to write responses to
	bool _bInitialized;
};

#endif //FIFO_HANDLER_H
