/*

The MIT License (MIT)

Copyright (c) 2013 ICVT Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#include <iostream>
#include <map>
#include <cassert>
#include <sys/mman.h>
#include <unistd.h>
#include <signal.h>
#include <tbb/tbb.h>
#include <tbb/parallel_for.h>
#include "tbb/tbb_stddef.h"
#include <thread>
#include "logger.h"
#include "memmgr.h"
#include "snapshot.h"
#include "hooks.h"
using namespace tbb;
using namespace std;
#define VALIDATE_MPROTECT_EXIT(r) \
if (r){\
	perror("mprotect");\
	cout << "Error: " << r << endl;\
	cout.flush();\
	exit(1);\
}

static int s_allocCnt = 0;
static volatile int s_segFaultCnt = 0;
static volatile int s_ignoredPageFaults = 0;
static long const s_pageAlign = sysconf(_SC_PAGESIZE);
typedef Snapshot::Clone Clone_t;
typedef Clone_t::Ptr_t Ptr_t;


/** : */
static inline Ptr_t AllocateClonePtr(void* ptr, size_t size) {
    Ptr_t clone(myfn_malloc(size) ,myfn_free, Allocator<void>()); // alignment does not matter because it's just a container.
    memcpy(clone.get(), ptr, size);
    return clone;
}

MemoryMgr::MemoryMgr() : _safe(true), _counter(0) {

}

MemoryMgr& MemoryMgr::instance() {
    static MemoryMgr mgr;
    return mgr;
}

MemoryMgr::~MemoryMgr() {
	_exit(0); //now!

	logger() << "[d'tor] Restoring Snapshot " << " Guarded Page Count: " << s_allocCnt << " Page Faults: " << s_segFaultCnt << " Ignored: " << s_ignoredPageFaults << endl;
	_snapshots.clear();

	// release memory that the user has previously free'd but we kept
	// around since it was in a snapshot 
	malloc_cont_iter_t it, e;
	for(it = _allocs.begin(), e = _allocs.end(); it != e; ++it) {
		if(!it->second._isLive) {
			myfn_free(it->first);
		}
	}
	_allocs.clear();
}

static inline void Release(void* ptr, size_t size) {
	int r = mprotect(ptr, size, PROT_READ | PROT_WRITE);
	VALIDATE_MPROTECT_EXIT(r);
}
/** : */
static inline void Protect(void* ptr, size_t size) {
	assert(!(size%s_pageAlign));
	assert(ptr == (void*)((unsigned long long)(ptr)&0xfffffffffffff000));

	int r = mprotect(ptr, size, PROT_READ);
	VALIDATE_MPROTECT_EXIT(r);

	s_allocCnt += size / s_pageAlign + ((size%s_pageAlign)? 1 : 0);
}

/** : */
void MemoryMgr::OnSegFault(void* addr, int serious) {
	// find the buffer in the protected map
	Allocs_t::iterator ptr = std::find_if(begin(_allocs), end(_allocs), [addr](Allocs_t::value_type& ptr) -> bool {
		return ((ptr.first <= addr) && (addr < (ptr.first + ptr.second._size)));
	});
	//assert(ptr != _allocs.end());
	if (ptr == _allocs.end()) {
		logger() << "ERROR: Ignoring segfault at : " << addr << endl;
		mprotect((void*)((unsigned long long)(addr)&0xfffffffffffff000), s_pageAlign, PROT_READ | PROT_WRITE);
		s_ignoredPageFaults++;
		//assert(false);
		return;
	}

	//s_segFaultCnt += size/s_pageAlign + ((size%s_pageAlign)? 1 : 0);
	s_segFaultCnt++;

	// copy the buffer to all relevant snapshots and release the memory
	//logger() << "Segfault at : " << addr << endl;
	CopyPtrToSnapshots(ptr);
}
void MemoryMgr::CopyPtrToSnapshots(Allocs_t::iterator& allocation) {
	void* addr = allocation ->first;
	PtrData& data = allocation ->second;
	// there must be at least one
	bool found = false;
	Ptr_t ptr = nullptr;
	
	for_each(begin(data._snapshots), end(data._snapshots), [&found, addr, &ptr, &data, &allocation] (PtrData::AttachedSnapshots_t::value_type s) {
		// this find can be removed by saving an iterator on the PtrData to his belonging on the MemMgr allocs table
		Snapshot::Clones_t::iterator clone = s ->_clones.find(addr);
		assert(clone != s ->_clones.end());
		if (!clone ->second._ptr){
		    if (!ptr) {
		        ptr = AllocateClonePtr(addr, data._size);
		        Release(allocation ->first, allocation->second._size);
		    }
	        //logger() << addr << " added to snapshot: " << s ->_id << endl;
			clone ->second._ptr = ptr;
			found = true;
		} else {
            //logger() << addr << " already exists in: " << s ->_id << endl;
		}
	});
	//assert(found);
}


void MemoryMgr::protectAllMemory() {
	for(malloc_cont_citer_t it = _allocs.begin(), e = _allocs.end(); it != e; ++it) {
		if (it ->second._protect && it->second._isLive) {
			Protect(it->first, it->second._size);
		}
	}
	logger() << "Protecting All memory: PageCount=" << s_allocCnt << " size= " << _allocs.size() << endl;
}

void MemoryMgr::resetAndReport() {
	for(Allocs_t::iterator it = _allocs.begin(), e = _allocs.end(); it != e; ++it) {
		if (it->second._protect) {
			int r = mprotect(it->first, it->second._size, PROT_READ | PROT_WRITE);
			VALIDATE_MPROTECT_EXIT(r);
		}
	}
	logger() << "Counted: " << s_allocCnt << " Faulted: " << s_segFaultCnt << " Ignored: " << s_ignoredPageFaults << endl;
	s_allocCnt = s_segFaultCnt = s_ignoredPageFaults = 0;
}

void MemoryMgr::AddPtr(void* ptr, size_t size, bool protect) {
	assert(!_safe || _allocs.find(ptr) == _allocs.end());
	_allocs[ptr] = PtrData(++_counter, size, protect);
	logger() <<"AddPtr: " << ptr << " Size: " << size << endl;
}

void MemoryMgr::onMalloc(void* ptr, size_t size) {
	AddPtr(ptr, size);
}

void MemoryMgr::onCalloc(void* ptr, size_t nmemb, size_t size) {
	AddPtr(ptr, nmemb * size);
}

void MemoryMgr::onMemAlign(void* ptr, size_t alignment, size_t size, bool protect) {
	AddPtr(ptr, size, protect);
}

void MemoryMgr::onPosixMemAlign(void* ptr, size_t alignment, size_t size) {
	AddPtr(ptr, size);
}

bool MemoryMgr::onFree(void* ptr) {
	bool shouldReallyFree = false;

	malloc_cont_iter_t it = _allocs.find(ptr); //check if allocation is tracked
	if(it != _allocs.end()) {
		//if tracked allocation belongs to any snapshots we cannot release it
		//instead, we mark it as dead
		if(it->second._snapshots.empty()) {
			logger() << ptr << " buffer not used in any snapshot releasing" << endl;
			_allocs.erase(it);
			shouldReallyFree = true;
		} else {
			it->second._isLive = false;
		}
	} else { 
		//sometimes memory might be allocated without us tracking it.
		//this could happen if memory was allocated using a method we don't override,
		//was allocated before the module was loaded, 
		//or the user did something funky.
		//in any case, if we're not tracking the ptr, the user can delete it
		shouldReallyFree = true;
	}

	assert (!shouldReallyFree || _allocs.find(ptr) == _allocs.end()); //verify free'd blocks are not tracked
	return shouldReallyFree;
}

size_t MemoryMgr::getMallocSize(void* ptr) const {
	malloc_cont_citer_t it = _allocs.find(ptr);
	if(it != _allocs.end()) {
		return it->second._size;
	}
	return 0;
}

enum Difference {
	CloneBigger,
	AllocBigger,
	Equal
};

// clone all live allocations, creating a "snapshot" that we can later restore
void MemoryMgr::takeSnapshot(int index) {
	pair<snapshots_cont_iter_t,bool> insRes = _snapshots.insert(Snapshots_t::value_type(index, Snapshot()));
	Snapshot& s = insRes.first->second;
	// first time we take snapshot so everything is set
	if (!_dummy_memory) {
	    _dummy_memory = AllocateClonePtr((void*)"stam", 4);
	}
	if(insRes.second) {
		logger() << " saving snapshot " << index << endl;
		malloc_cont_iter_t it = _allocs.begin(), e = _allocs.end();
		for(; it != e; ++it) {
			AddClone(s, it);
		}
	} else {
		// this is based on std::map being sorted, so we can iterate like 2 sorted lists merges in o(m+n) without creating sub lists
		malloc_cont_iter_t alloc = _allocs.begin(), endAllocs = _allocs.end();
		Snapshot::Clones_t::iterator clone = s._clones.begin(), endClones = s._clones.end();
		logger() << "updating existing allocs size: " << _allocs.size() << "snapshot size: " << s._clones.size() << endl;
		while(clone != endClones && alloc != endAllocs) {
			Difference diff = clone->first < alloc->first ? AllocBigger : (clone->first > alloc->first ? CloneBigger : Equal);

			switch (diff){
			case CloneBigger:
				// new allocation that was not saved in this snapshot yet, we can simply add it
				AddClone(s, alloc);
				++alloc;
				break;
			case Equal:
				// erase first for easiness, ptrdata only has a map of like size 10 max(amount of snapshots)
				if (!alloc ->second._isLive) {
					alloc ->second._snapshots.erase(&s);
					if (alloc ->second._snapshots.empty()) {
						if (alloc ->second._protect) {
							Release(alloc->first, alloc->second._size);
						}
						myfn_free(alloc->first);
						alloc = _allocs.erase(alloc);
					} else {
						++alloc;
					}
					clone = s._clones.erase(clone);
				} else { // ptr already existed.. maybe it's different maybe it's not..
					if (alloc ->second._protect) {
						// reset the shared_ptr.. if other snapshots pointed to it, it will remain
						clone->second._ptr.reset();
					} else {
						// must be equal or it would of been released
						if (!s.updateBuffer(alloc ->first, alloc ->second._size, clone)) {
							clone ->second._ptr.reset();
							clone ->second._ptr = AllocateClonePtr(alloc ->first, alloc ->second._size);
							clone ->second._size = alloc ->second._size;
						}
					}
					++alloc;
					++clone;
				}
				break;
			default:// AllocBigger:
				// this means there is a clone that does not have a link in allocation
				// it can only happen if either the data structure was changed(not sorted)
				// or there is a bug
				assert(false);
				break;
			}
		}

		for ( ; alloc != endAllocs ; ++alloc) {
			AddClone(s, alloc);
		}

		assert(clone == endClones);// there must not be any remaining clones
	}

#ifdef DUMP_SNAPSHOT_STATS
		ofstream dump;
		dump.open("/tmp/snapshot_stats.log", ios::out | ios::app);
		dump << "updated: " << updatedBuffers << " added: " << addedBuffers << " removed: " << removedBuffers << " skipped: " << skippedBuffers << endl;
#endif
	protectAllMemory();
}

// copy data from snapshot back into original buffers
void MemoryMgr::restoreSnapshot(int index) {
	logger() << "Counted: " << s_allocCnt << " Faulted: " << s_segFaultCnt << " Ignored: " << s_ignoredPageFaults << endl;
	s_allocCnt = s_segFaultCnt = s_ignoredPageFaults = 0;
	//find snapshot
	snapshots_cont_iter_t it1 = _snapshots.find(index);
	assert (!_safe || it1 != _snapshots.end());
	Snapshot const& s = it1->second;
	// unprotect?..

	// restore data of each buffer from snapshot to it's original ptr
	// update the _isLive flag of each allocation to indicate if the user is maintaining 
	// a reference to this buffer
	logger() << "restoring snapshot " << index << endl;
	//SetSteadyState();
	// Parenthesis so for dll unload the scheduler will be released and then steady state will be activated
	{
		//static tbb::task_scheduler_init init(max(1, tbb::task_scheduler_init::default_num_threads() - 1));
		/*parallel_*/for_each(begin(_allocs), end(_allocs), [&s] (Allocs_t::value_type& allocation) {

			void *ptr = allocation.first;
			PtrData& data = allocation.second;
			Snapshot::snapshot_clones_citer_t clone;
			if(s.hadBuffer(ptr, &clone)) { // TODO; think of islive not changed but back
				s.restoreBuffer(clone);
				data._isLive = true;
			} else {
				data._isLive =  false;
			}
		});
	}
	//ReleaseSteadyState();
}

void MemoryMgr::releaseSnapshot(int index) {
	logger() << "releasing snapshot " << index << endl;
	snapshots_cont_iter_t it1 = _snapshots.find(index);
	assert (!_safe || it1 != _snapshots.end());
	_snapshots.erase(it1);
}

void MemoryMgr::releaseAllSnapshots() {
	logger() << "releasing all snapshots" << endl;
	resetAndReport();
	for(auto& snapshot : _snapshots) {
	    for(auto& clone : snapshot.second._clones) {
	        if (!clone.second._ptr) {
	            clone.second._ptr = _dummy_memory;
	        }
	    }
	}
}

void MemoryMgr::removeFromSnapshot(void* ptr, const Snapshot *snapshot) {
	//find ptr in _allocs in order to get PtrData reference
	logger() << "Going to remove " << ptr << " from snapshot " << snapshot << endl;
	malloc_cont_iter_t it = _allocs.find(ptr);
	assert(it != _allocs.end());
	logger() << "found original ptr and data" << endl;

	//update snapshots in PtrData, removing snapshot reference
	PtrData &p = it->second;
	PtrData::snapshot_groups_iter_t it2;

	for(it2=p._snapshots.begin(); it2!=p._snapshots.end(); ++it2) {
		Snapshot* snap = *it2;
		if(snap == snapshot) {
			logger() << "removed snapshot" << endl;
			p._snapshots.erase(it2);
			break;
		}
	}
	logger() << "ptr remains in " << p._snapshots.size() << " snapshots" << endl;

	//remove this ptrData if it is no longer used
	if (p._snapshots.empty()) {
		if (it->second._protect) {
			Release(it->first, it->second._size);
		}
		if (!p._isLive) {
			logger() << "ptr is dead, so we're releasing it" << endl;
			// no longer keeping track either
			myfn_free(it->first);
			_allocs.erase(it);
		} else {
			logger() << "ptr is live, so we need to keep it around" << endl;
		}
	} else {
		//there might be another bug, not sure if it's here, but in case it's not empty, needed to be protected, but was already copied and re-protected
		logger() << "ptr is live, so we need to keep it around" << endl;
	}
}


void MemoryMgr::clear() {
	_snapshots.clear();
	_allocs.clear();
}


// Dump sorted list of allocations, and snapshots 
// the allocations are ordered by their id
void MemoryMgr::dump(const char* filename) const {

	ofstream dump;
	dump.open(filename, ios::out | ios::trunc);

	// sort all alloctaions by id
	typedef Allocator<std::pair<unsigned long const, malloc_cont_citer_t> > sorting_allocator_t;
	map<unsigned long, malloc_cont_citer_t, std::less<unsigned long>, sorting_allocator_t> sorted;
	malloc_cont_citer_t it;
	for(it=_allocs.cbegin(); it!=_allocs.cend(); ++it) {
		sorted[it->second._id] = it;
	}

	// for each allocation, dump allocation info and list of snapshots 
	map<unsigned long, malloc_cont_citer_t>::const_iterator it2;
	for(it2=sorted.cbegin(); it2!=sorted.cend(); ++it2) {
		const malloc_cont_citer_t& it3 = it2->second;
		dump << "Ptr: " << it3->second._id << " size: " << it3->second._size << " live: " << it3->second._isLive << ' ' << endl;
		PtrData::snapshot_groups_citer_t it4;
		for(it4=it3->second._snapshots.cbegin(); it4!=it3->second._snapshots.cend(); ++it4) {
			Snapshot* snapshot = *it4;
			void* ptr = it2->second->first;
			dump << "Snap: " << snapshot << " crc: " << snapshot->get_checksum(ptr) << endl;
		}
	}
}

unsigned int MemoryMgr::getAllocId(void *ptr) const {
	malloc_cont_citer_t it = _allocs.find(ptr);
	assert(it != _allocs.end());
	return it->second._id;
}

// Dump sorted list of live allocations, and their data
// the allocations are ordered by their id
void MemoryMgr::dumpData(const char* filename, size_t minSize) const {
	ofstream dump;
	dump.open(filename, ios::out | ios::trunc | ios::binary);

	typedef Allocator<std::pair<unsigned long const, malloc_cont_citer_t> > sorting_allocator_t;
	map<unsigned long, malloc_cont_citer_t, std::less<unsigned long>, sorting_allocator_t> sorted;
	malloc_cont_citer_t it;
	for(it=_allocs.cbegin(); it!=_allocs.cend(); ++it) {
		if(it->second._isLive && it->second._size > minSize) {
			sorted[it->second._id] = it;
		}
	}

	map<unsigned long, malloc_cont_citer_t>::const_iterator it2;
	for(it2=sorted.cbegin(); it2!=sorted.cend(); ++it2) {
		const malloc_cont_citer_t& it3 = it2->second;
		void* ptr = it3->first;
		size_t size = it3->second._size;
		dump << it3->second._size;
		dump.write(static_cast<char*>(ptr), size);
	}

	dump.close();
}

void MemoryMgr::dump(const char* filename, int index) const {
	const Snapshot& s = _snapshots.find(index)->second;
	s.dump(filename);
}

bool MemoryMgr::isAlloced(void *ptr) const {
	return _allocs.find(ptr) != _allocs.end();
}

bool MemoryMgr::isLive(void *ptr) const {
	return _allocs.find(ptr)->second._isLive;
}

int MemoryMgr::countSnapshots(void *ptr) const {
	return _allocs.find(ptr)->second._snapshots.size();
}

void MemoryMgr::AddClone(Snapshot& to, AllocIterator_t& alloc) {
	PtrData& data = alloc->second;
	void* addr = alloc->first;
	if(data._isLive) {

		Ptr_t ptr(nullptr);
		if (!data._protect) {
			ptr = AllocateClonePtr(addr, data._size);
            logger() << addr << " Copied to snapshot: " << to._id << endl;
		} else {
		    logger() << addr << " Protected by snapshot: " << to._id << endl;
		}
		to.addBuffer(addr, Clone_t(data._size, ptr, alloc));
		data._snapshots.insert(&to);
	}
}
