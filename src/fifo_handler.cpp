/*

The MIT License (MIT)

Copyright (c) 2013 ICVT Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#ifdef USE_FIFO_HANDLER

using namespace std;

#include <iostream>
#include <fstream>
#include <thread>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sstream>
#include "memmgr.h"
#include "fifo_handler.h"

using namespace std;

void sighandler(int signum, siginfo_t *info, void *ptr) {
	logger() << "received signal: " << signum << endl;
}

bool starts_with(const char* s1, const char* s2) {
	int l1 = strlen(s1);
	int l2 = strlen(s2);
	return  l2 <= l1 && strncmp(s1, s2, l2) == 0;
}

const char* substr(const char* s, int index) {
	return s+index;
}

bool check_file_exists(const std::string& name) {
	struct stat buffer;   
	return (stat (name.c_str(), &buffer) == 0); 
}

FIFOHandler::FIFOHandler() : _reader_thread(NULL), _bInitialized(false) {
}

/*
Create new fifos, and then open them in their own threads. 
Wait for all fifos to open 
*/
void FIFOHandler::init() {
	// register signal handler for SIGPIPE
	static struct sigaction act;
	static sigset_t block_mask;
	memset(&act, 0, sizeof(act));
	sigemptyset(&block_mask);
	sigfillset(&block_mask);	//block all other signals during our signal handler
	act.sa_sigaction = sighandler;
	act.sa_flags = SA_SIGINFO;
	act.sa_mask = block_mask;
	sigaddset(&act.sa_mask, SIGPIPE);
	sigaction(SIGPIPE, &act, NULL);

	// create PIPEs
	long pid = (long)getpid();
	stringstream i, o;
	const char* prefix = getenv("MMGR_FIFO_PATH") ? getenv("MMGR_FIFO_PATH") : "/tmp";
	i << prefix << "/memmgr." << pid << ".in" << ends;
	o << prefix << "/memmgr." << pid << ".out" << ends;
	_reader_name = i.str();
	_writer_name = o.str();

	const char* sz_reader_name = _reader_name.c_str();
	const char* sz_writer_name = _writer_name.c_str();

	if (check_file_exists(_reader_name)) {
		cerr << _reader_name << " already exists. removing it" << endl;
		if (remove(sz_reader_name) != 0) {
			cerr << "failed to remove " << _reader_name << endl;
		}
	}

	if(0 != mkfifo(sz_reader_name, S_IWUSR | S_IRUSR)) {
		cerr << "Error " << errno << " when calling mkfifo " << _reader_name << endl;
	}

	if (check_file_exists(_writer_name)) {
		cerr << _writer_name << " already exists. removing it" << endl;
		if (remove(sz_writer_name) != 0) {
			cerr << "failed to remove " << _writer_name << endl;
		}
	}

	if (0 != mkfifo(sz_writer_name, S_IWUSR | S_IRUSR)) {
		cerr << "Error " << errno << " when calling mkfifo " << _writer_name << endl;

	}

	thread t1(FIFOOpener<ifstream>(_reader, _reader_name, ios::in));
	thread t2(FIFOOpener<ofstream>(_writer, _writer_name, ios::out));
	t1.join();
	t2.join();

	_bInitialized = true;
}


/*
Wait for all threads to finish, 
delete fifos 
*/
FIFOHandler::~FIFOHandler() {
	if(_reader_thread) {
		logger() << "Waiting for reader thread to terminate" << endl;
		_reader_thread->detach();
		delete(_reader_thread);
		_reader_thread = NULL;
	}

	logger() << "cleaning up fifos" << endl;
	if(_reader.is_open()) {
		_reader.close();
	}
	if(_writer.is_open()) {
		_writer.close();
	}
	remove(_reader_name.c_str());
	remove(_writer_name.c_str());
}


FIFOHandler& FIFOHandler::instance() {
	static FIFOHandler obj;
	return obj;
}


/*
Main input loop - 
    read from input fifo (or block) 
    parse command, and send to memory_mgr
    write result to output fifo (
 
    This method will finish when "exit\n" is received or the input fifo is closed
*/
void FIFOHandler::operator()() {
	if(!_bInitialized) {
		init();
	}

	char buf[256];
	while(_bInitialized) { //if initialization failed, exit
		_reader.getline(buf, 256);
		char* cmd = buf;
		logger() << "received command: " << cmd << endl;

		if(starts_with(cmd, "reset")) {
			logger() << "resetting all snapshots" << endl;
			MemoryMgr::instance().releaseAllSnapshots();
			_writer << "done" << endl;
			_writer.flush();
		} else if(starts_with(cmd, "snapshot")) {
			int slot = atoi(substr(cmd, 9));
			logger() << "taking snapshot " << slot << endl;
			MemoryMgr::instance().takeSnapshot(slot);
			_writer << "done" << endl;
			_writer.flush();
		} else if(starts_with(cmd, "restore")) {
			int slot = atoi(substr(cmd, 8));
			logger() << "restoring snapshot " << slot << endl;
			MemoryMgr::instance().restoreSnapshot(slot);
			_writer << "done" << endl;
			_writer.flush();
		} else if(starts_with(cmd, "dump")) {
			logger() << "dumping data to " << substr(cmd, 5) << endl;
			MemoryMgr::instance().dump(substr(cmd, 5));
			_writer << "done" << endl;
			_writer.flush();
		} /*else if(starts_with(cmd, "ddump")) {
			size_t t1 = cmd.find_first_of(" ");
			size_t t2 = cmd.find_last_of(" ");
			int minSize = atoi(cmd.substr(t2).c_str());
			std::string name = cmd.substr(t1+1,t2-t1-1);
			logger() << "dumping data of size " << minSize << " to " << name << endl;
			MemoryMgr::instance().dumpData(name.c_str(), minSize);
			_writer << "done" << endl;
			_writer.flush();
		} */ else if (starts_with(cmd, "exit")) {
			logger() << "exiting" << endl;
			_writer << "done" << endl;
			_writer.flush();
			break;
		}
		if(_reader.eof()) {
			logger() << "EOF. Exiting." << endl;
			break;
		}
	}
}

void FIFOHandler::start() {
	//in case we need to call cerr (even for debugging), we need to make sure it's initialized first
	static std::ios_base::Init initializer; 
	init();
	_reader_thread  = new thread(std::ref(FIFOHandler::instance())); //keep thread alive after function exists
}

int init_fifo_handler() {
	FIFOHandler::instance().start();
	return 0;
}

#endif //USE_FIFO_HANDLER
