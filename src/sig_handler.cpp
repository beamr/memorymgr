/*

The MIT License (MIT)

Copyright (c) 2013 ICVT Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#ifdef USE_SIGNAL_HANDLER

#include <stdio.h>
#include <signal.h>
#include <iostream>
#include <memory.h>
#include <stdlib.h>
#include <string>
#include "memmgr.h"
#include "logger.h"

using namespace std;

extern pthread_mutex_t* getMutex();

int g_last_signum_handled = 0;	//used for testing
static long g_sig_count = 0;

/*
Receive signals from external process 
Send commands to memory_mgr 
 
	SIGUSR1 - release all snapshots 
	SIGRTMIN - take snapshot 0 
	SIGRTMIN+1 - restore snapshot 0
    SIGRTMIN+2 - take snapshot 1
    SIGRTMIN+2 - restore snapshot 1
    ...
	...
    SIGRTMAX - 1 - restore snapshot n
 
The above are used since there is no way to send parameters with signals 
*/
void sighandler(int signum, siginfo_t *info, void *ptr) {
	g_last_signum_handled = signum;
	if(signum == SIGUSR1) {
		logger() << "Received SIGUSR1 - releasing all snapshots" << endl;
		MemoryMgr::instance().releaseAllSnapshots();
	} else if(signum == SIGUSR2) {
		logger() << "Received SIGUSR2 - unimplemented" << endl;
	} else if(signum >= SIGRTMIN && signum < SIGRTMAX) {
		logger() << g_sig_count++ << " Received RT signal " << signum << endl;
		int slot = (signum - SIGRTMIN) / 2;
		logger() << "Set snapshot index to " << slot << endl;
		if((signum - SIGRTMIN) % 2 == 0) {
			MemoryMgr::instance().takeSnapshot(slot);
		} else {
			MemoryMgr::instance().restoreSnapshot(slot);
		}
	}
}

/*
Initialize signal handlers
*/
void init_signal_handler() {
	static struct sigaction act;
	static sigset_t block_mask;
	memset(&act, 0, sizeof(act));
	sigemptyset(&block_mask);
	sigfillset(&block_mask);	//block all other signals during our signal handler

	act.sa_sigaction = sighandler;
	act.sa_flags = SA_SIGINFO;
	act.sa_mask = block_mask;

	sigaddset(&act.sa_mask, SIGUSR1);
	sigaddset(&act.sa_mask, SIGUSR2);
	sigaction(SIGUSR1, &act, NULL);
	sigaction(SIGUSR2, &act, NULL);

	// register RT signal handlers
	for(int i = SIGRTMIN; i < SIGRTMAX; ++i) {
		sigaddset(&act.sa_mask, i);
		sigaction(i, &act, NULL);
	}
}

#endif //USE_SIGNAL_HANDLER
