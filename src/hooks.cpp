/*

The MIT License (MIT)

Copyright (c) 2013 ICVT Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/


#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/mman.h>
#include <signal.h>

#ifndef __USE_GNU
#define __USE_GNU
#endif

#include <dlfcn.h>
#include "memmgr.h"

extern void abort();

#ifdef USE_SIGNAL_HANDLER
extern void init_signal_handler();
#endif

#ifdef USE_FIFO_HANDLER
extern void init_fifo_handler();
#endif

void* (*myfn_calloc)(size_t nmemb, size_t size) = NULL;
void* (*myfn_malloc)(size_t size) = NULL;
void* (*myfn_realloc)(void *ptr, size_t size) = NULL;
void* (*myfn_memalign)(size_t alignment, size_t size) = NULL;
void (*myfn_free)(void *ptr) = NULL;
int (*myfn_posix_memalign)(void **memptr, size_t alignment, size_t size) = NULL;
int (*myfn__cxa_atexit)(void (*func) (void *), void *arg, void *d) = NULL;
size_t (*myfn_fread)(void *buf, size_t size, size_t elements, FILE *fp) = NULL;
ssize_t (*myfn_pread)(int fildes, void *buf, size_t nbyte, off_t offset) = NULL;
ssize_t (*myfn_read)(int fildes, void *buf, size_t nbyte) = NULL;

static int s_steadyState = 0;
static long const s_minPagingMultiplier = 4;
static long s_pageAlign = 0;
static long s_minPaging = 0;
static void* temporary_calloc(size_t nmemb, size_t size) {
	//http://blog.bigpixel.ro/2010/09/interposing-calloc-on-linux/
	return NULL;
}

pthread_mutex_t* getMutex();
static void handler(int sig, siginfo_t *si, void *unused) {
    static MemoryMgr& mgr = MemoryMgr::instance();
    pthread_mutex_lock(getMutex());
    mgr.OnSegFault(si ->si_addr, sig);
    pthread_mutex_unlock(getMutex());
}

/*
getMutex is called at the start of each overriding function below. 
Because the memorymgr is typically used as a preloaded library, this is the place 
to initialize the library (there is no dllstart function called by the system) 
 
Note that inside the overridden functions it is safe only to assume that function static 
variables and objects have been created and intialized. The state of other globals is not known.
*/
pthread_mutex_t* getMutex() {
	static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
	static pthread_mutex_t mutex2;
	static pthread_mutexattr_t mta;
	static bool init = false;

	if(!init) {
		pthread_mutex_lock(&mutex);
	    if (!s_pageAlign) {
	        s_pageAlign = sysconf(_SC_PAGESIZE);
            s_minPaging = s_pageAlign*s_minPagingMultiplier;
	    }
		if(!init) {
			pthread_mutexattr_init(&mta);
			pthread_mutexattr_settype(&mta, PTHREAD_MUTEX_RECURSIVE); //allow single thread to call
			pthread_mutex_init(&mutex2, &mta);						  //mutex without blocking
			init = true;

	        //DO NOT log any messages here, as these might call new, and create
	        //a deadlock inside the signal malloc handler
	        struct sigaction sa;

	        sa.sa_flags = SA_SIGINFO | SA_NOCLDWAIT;
	        sigemptyset(&sa.sa_mask);
	        sa.sa_sigaction = &handler;
	        if (sigaction(SIGSEGV, &sa, NULL) == -1)
	            perror("sigaction");
#ifdef USE_SIGNAL_HANDLER
			init_signal_handler();
#endif

#ifdef USE_FIFO_HANDLER
			init_fifo_handler();
#endif
		}
		pthread_mutex_unlock(&mutex);
	}

	return &mutex2;
}

#ifndef DISABLE_OVERRIDE
	#define xmalloc malloc
	#define xfree free
	#define xrealloc realloc
	#define xcalloc calloc
	#define xmemalign memalign
	#define xposix_memalign posix_memalign
	#define xfread fread
	#define xpread pread
	#define xread read
	#define xcxa_atexit __cxa_atexit
#else
	#define xmalloc mmgr_malloc
        #define xfree mmgr_free
        #define xrealloc mmgr_realloc
        #define xcalloc mmgr_calloc
	#define xmemalign memalign
	#define xposix_memalign posix_memalign
	#define xfread mmgr_fread
        #define xpread mmgr_pread
        #define xread mmgr_read
        #define xcxa_atexit mmgr__cxa_atexit
#endif

extern "C" {


void *xmalloc(size_t size) {
	pthread_mutex_lock(getMutex());

	//if we don't yet have a reference to the original malloc, get it
	if(!myfn_malloc) {
		myfn_malloc = (void* (*)(size_t))dlsym(RTLD_NEXT, "malloc");
	}

	if(!myfn_memalign) {
		myfn_memalign = (void* (*)(size_t, size_t))dlsym(RTLD_NEXT, "memalign");
	}
	assert(myfn_malloc);
	assert(myfn_memalign);

	void *alloc;
	if (size > s_minPaging) {
		size += 0xfff;
		size &= 0xfffffffffffff000;
		alloc = myfn_memalign(s_pageAlign, size);
		if(alloc && !s_steadyState) {
			MemoryMgr::instance().onMemAlign(alloc, s_pageAlign, size, true);
		}
	} else {
		//call the original malloc to allocate the memory from the system
		alloc = myfn_malloc(size);
		//track the returned ptr
		if(alloc && !s_steadyState) {
			MemoryMgr::instance().onMalloc(alloc, size);
			#ifdef DEBUG
			memset(alloc, 0, size);
			#endif
		}
	}

	pthread_mutex_unlock(getMutex());
	return alloc;
}

void xfree(void* ptr) {
	pthread_mutex_lock(getMutex());

	if(!myfn_free) {
		myfn_free = (void (*)(void*))dlsym(RTLD_NEXT, "free");
	}
	assert(myfn_free);

	//check if the memory mgr needs to keep the ptr around
	//and only release the memory if the ptr is not not used in any snapshot.
	//Note - as far as the user is concerend, the memory is relased
	if(ptr) {
		if(MemoryMgr::instance().onFree(ptr)) { 
			myfn_free(ptr); //release the memory
		}
	}

	pthread_mutex_unlock(getMutex());
}


void *xrealloc(void *ptr, size_t size) {
	pthread_mutex_lock(getMutex());

	if(!myfn_realloc) {
		myfn_realloc = (void* (*)(void*, size_t))dlsym(RTLD_NEXT, "realloc");
	}
	assert(myfn_realloc);
	void *alloc = xmalloc(size);
	if(alloc && ptr) {
		memcpy(alloc, ptr, std::min(MemoryMgr::instance().getMallocSize(ptr), size));       
	}
	if(ptr) {
		xfree(ptr);
	}

	pthread_mutex_unlock(getMutex());
	return alloc;
}

void *xmemalign(size_t alignment, size_t size) {
	pthread_mutex_lock(getMutex());

	if(!myfn_memalign) {
		myfn_memalign = (void* (*)(size_t, size_t))dlsym(RTLD_NEXT, "memalign");
	}
	assert(myfn_memalign);

	bool protect = false;
	if (size > s_minPaging) {
		alignment = s_pageAlign;
		size += 0xfff;
		size &= 0xfffffffffffff000;
		protect = true;
	}

	void *alloc = myfn_memalign(alignment, size);
	if(alloc && !s_steadyState) {
		MemoryMgr::instance().onMemAlign(alloc, alignment, size, protect);
#ifdef DEBUG
		memset(alloc, 0, size);
#endif
	}

	pthread_mutex_unlock(getMutex());
	return alloc;
}


int xposix_memalign(void **memptr, size_t alignment, size_t size) {
	pthread_mutex_lock(getMutex());

	if(!myfn_posix_memalign) {
		myfn_posix_memalign = (int (*)(void**, size_t, size_t))dlsym(RTLD_NEXT, "posix_memalign");
	}

	assert(myfn_posix_memalign);

	int res = myfn_posix_memalign(memptr, alignment, size);
	if(res == 0 && !s_steadyState) {
		MemoryMgr::instance().onPosixMemAlign(*memptr, alignment, size);
#ifdef DEBUG
		memset(*memptr, 0, size);
#endif
	}

	pthread_mutex_unlock(getMutex());
	return res;
}

void *xcalloc(size_t nmemb, size_t size) {
	if(!myfn_calloc) {
		myfn_calloc = temporary_calloc;
        myfn_calloc = (void* (*)(size_t, size_t))dlsym(RTLD_NEXT, "calloc");
    }
	assert(myfn_calloc);

	if (s_steadyState) {
		return myfn_calloc(nmemb, size);
	}
	pthread_mutex_lock(getMutex());

	//turns out that dlsym calls calloc which results in a loop
	//follow the tip below, we provide a temporary_calloc that returns null
	//and assign it to myfn_calloc before calling the dlsym to get the original calloc
	//http://blog.bigpixel.ro/2010/09/interposing-calloc-on-linux/
	if(!myfn_memalign) {
		myfn_memalign = (void* (*)(size_t, size_t))dlsym(RTLD_NEXT, "memalign");
	}
	assert(myfn_memalign);
	assert(myfn_calloc);

	void* alloc;
	if (size*nmemb > s_minPaging) {// if we really want we can remove this if
		size*= nmemb;
		size += 0xfff;
		size &= 0xfffffffffffff000;
		alloc = myfn_memalign(s_pageAlign, size);
		if(alloc) {
			MemoryMgr::instance().onMemAlign(alloc, s_pageAlign, size, true);
		}
	} else {
		alloc = myfn_calloc(nmemb, size);
		if(alloc) {
			MemoryMgr::instance().onCalloc(alloc, nmemb, size);
			#ifdef DEBUG
			memset(alloc, 0, size);
			#endif
		}
	}

	pthread_mutex_unlock(getMutex());
	return alloc;
}

size_t xfread(void *buf, size_t size, size_t elements, FILE *fp) {
	if(!myfn_fread) {
		myfn_fread = (size_t (*)(void *buf, size_t size, size_t elements, FILE *fp))dlsym(RTLD_NEXT, "fread");
	}
	if (size) {
		*(char*)buf = 0;
	}
	return myfn_fread(buf, size, elements, fp);
}

ssize_t xpread(int fildes, void *buf, size_t nbyte, off_t offset) {
	if(!myfn_pread) {
		myfn_pread = (ssize_t (*)(int fildes, void *buf, size_t nbyte, off_t offset))dlsym(RTLD_NEXT, "pread");
	}
	if (nbyte) {
		*(char*)buf = 0;
	}
	return myfn_pread(fildes, buf, nbyte, offset);
}

ssize_t xread(int fildes, void *buf, size_t nbyte) {
	if(!myfn_read) {
		myfn_read = (ssize_t (*)(int fildes, void *buf, size_t nbyte))dlsym(RTLD_NEXT, "read");
	}

	if (nbyte) {
		*(char*)buf = 0;
	}
	return myfn_read(fildes, buf, nbyte);
}
int xcxa_atexit (void (*func) (void *), void *arg, void *d) {
	int res = 0;
	s_steadyState = 1;
	
	if(!myfn__cxa_atexit) {
		myfn__cxa_atexit = (int (*)(void (*) (void*), void *, void*))dlsym(RTLD_NEXT, "__cxa_atexit");
	}

	res = myfn__cxa_atexit(func, arg, d);
	s_steadyState = 0;
    return res;
}

void SetSteadyState() { s_steadyState = 1;}
void ReleaseSteadyState() { s_steadyState = 0; }
}

