/*

The MIT License (MIT)

Copyright (c) 2013 ICVT Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#include "snapshot.h"
#include <iostream>
#include <sys/mman.h>
#include <cassert>
#include <cstring> //memcpy
#include <random>
#include <iostream>
#include <fstream>
#include "hooks.h"
#include "logger.h"
#include "memmgr.h"

using namespace std;

extern uint16_t gen_crc16(const uint8_t *data, uint16_t size);

#define steps 10000
#define radius 10
int fast_memcmp(void* a, void* b, size_t s) {
#ifdef DUMP_SNAPSHOT_STATS
	ofstream dump;

if(s>1000000) {

	dump.open("/tmp/memcmp.log", ios::out | ios::app);
}
#endif

	for (size_t i = 0; i < s/steps; ++i) {
		int r = memcmp(a+i*steps, b+i*steps, radius);
		if(r) {
#ifdef DUMP_SNAPSHOT_STATS
			if(s>1000000) {
				dump << "memcmp of " << s << " bytes saved after " << i << " iterations " << endl;
			}
#endif
			return r;
		}
	}
	int r = memcmp(a, b, s);
#ifdef DUMP_SNAPSHOT_STATS
if(s>1000000) {
	dump << "memcmp of " << s << " bytes returned " << r << endl;
}
#endif
	return r;
}

Snapshot::Snapshot() : _id(rand()) {
	logger() << "Created snapshot " << _id << endl;
	if(!myfn_malloc) {
		myfn_malloc = (void* (*)(size_t))dlsym(RTLD_NEXT, "malloc");
	}
	assert(myfn_malloc);
	if(!myfn_free) {
		myfn_free = (void (*)(void*))dlsym(RTLD_NEXT, "free");
	}
	assert(myfn_free);
	if(!myfn_memalign) {
		myfn_memalign = (void* (*)(size_t, size_t))dlsym(RTLD_NEXT, "memalign");
	}
	assert(myfn_memalign);
}

Snapshot::~Snapshot() {
	logger() << "Removing snapshot " << _id << " with " << _clones.size() << " elements" << endl;
	snapshot_clones_iter_t it, e;
	int i = 0;
	while (!_clones.empty()) {
		it = _clones.begin();
		++i;
		//logger() << "going to remove ptr " << it->first << " from snapshot " << this << endl;
		removeBuffer(it);
	}
	logger() << i << " buffers released from memory" << endl;
}

void Snapshot::removeBuffer(void *ptr) {
	//logger() << "going to remove ptr " << ptr << " from snapshot " << this << endl;
	removeBuffer(_clones.find(ptr));
}

void Snapshot::removeBuffer(snapshot_clones_iter_t it) {
	assert(it != _clones.end());
	if (it->second._ptr) {
		//logger() << "releasing memory at " << it->second._ptr<< endl;
		it->second._ptr.reset();
	}
	//logger() << "unmarking ptr for snapshot" << endl;
	MemoryMgr::instance().removeFromSnapshot(it->first, this);

	//logger() << "removing ptr from snapshot allocs" << endl;
	_clones.erase(it);
}

void Snapshot::addBuffer(void* ptr, Clone const& clone) {
	//logger() << "adding ptr " << ptr << " with buffer size " << clone._size << " to snapshot " << this << endl;	// currently assuming all allocated memory is memaligned to page size

	_clones.insert(Clones_t::value_type(ptr, clone));
}

bool Snapshot::hadBuffer(void* ptr, snapshot_clones_citer_t* it) const {
	snapshot_clones_citer_t res = _clones.find(ptr);
	if (it) {
		*it = res;
	}
	return res != _clones.end();
}

void Snapshot::restoreBuffer(snapshot_clones_citer_t& it) const {
	//logger() << "restoring ptr " << it->first << endl;

	if (!it->second._ptr) {
		//logger() << "No need to restore ptr at " << it->first << endl;
		return;
	}
	assert(it != _clones.end());
	//logger() << "restoring " << it->second._size << " bytes from: " << it->second._ptr << endl;

	if(fast_memcmp(it->first, it->second._ptr.get(), it->second._size) != 0) {
		memcpy(it->first, it->second._ptr.get(), it->second._size); //copy from cloned buffer to user's ptr
	}
}

/*
    update data in clone's buffer
 
    returns:
    0 - buffer wasn't changed, no need to update
    1 - buffer changed, new data copied
    2 - buffer and size changed, clone resized and copeid
*/
bool Snapshot::updateBuffer(void* ptr, size_t size, Clones_t::iterator& it) {
	Clone& clone = it->second;
	assert(size == clone._size);
	if(size == clone._size) {
		if(fast_memcmp(clone._ptr.get(), ptr, size) == 0) {
			return true;
		} else {
			memcpy(clone._ptr.get(), ptr, size);
			return true;
		}
	}
	return false;
}

uint16_t Snapshot::get_checksum(void* ptr) const {
	snapshot_clones_citer_t it = _clones.find(ptr);
	if(it != _clones.end()) {
		return gen_crc16(static_cast<const uint8_t*>(it->second._ptr.get()), it->second._size);
	} else {
		return 0;
	}
}

bool Snapshot::isDirty(void* ptr) const {
	Clone clone = _clones.find(ptr)->second;
	return (memcmp(ptr, clone._ptr.get(),  clone._size) != 0);
}

void Snapshot::dump(const char* filename) const {
	ofstream dump;
	dump.open(filename, ios::out | ios::app);

	dump << "Items: " << _clones.size() << endl;
	snapshot_clones_citer_t it, e;
	for(it = _clones.cbegin(), e = _clones.cend(); it != e; ++it) {
		const Clone& clone = it->second;
		dump << clone._size << ",";
	}
	dump << endl;
}

bool operator==(const Snapshot &lhs, const Snapshot &rhs) { 
	return lhs._id == rhs._id; 

}

