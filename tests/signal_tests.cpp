/*

The MIT License (MIT)

Copyright (c) 2013 ICVT Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#ifdef USE_SIGNAL_HANDLER

	#include <iostream>
	#include <sys/types.h>
	#include <signal.h>
	#include <unistd.h>
	#include "signal_tests.h"
	#include "memmgr.h"

using namespace std;

extern int g_last_signum_handled;

void SignalTests::setUp() {
}

void SignalTests::tearDown() {
}

void SignalTests::TestUSR1() {
	kill(getpid(), SIGUSR1);
	CPPUNIT_ASSERT(g_last_signum_handled == SIGUSR1);
	CPPUNIT_ASSERT(MemoryMgr::instance().snapshotCount() == 0);
}

void SignalTests::TestUSR2() {
	kill(getpid(), SIGUSR2);
	CPPUNIT_ASSERT(g_last_signum_handled == SIGUSR2);
}

void SignalTests::TestRT() {
	//cerr << "Clearing all snapshots" << endl;
	kill(getpid(), SIGUSR1); //clear all snapshots
	for(int i=SIGRTMIN; i<SIGRTMAX; i++) {
		//cerr << "Sending signal " << i << endl;
		kill(getpid(), i); //store or restore a snapshot
		//cerr << "Checking if last signal (" << g_last_signum_handled << ") is the same as " << i << endl;
		CPPUNIT_ASSERT(g_last_signum_handled == i);
	}
	//cerr << "Verifying total number of snapshots" << endl;
	CPPUNIT_ASSERT(MemoryMgr::instance().snapshotCount() == ((unsigned int)(SIGRTMAX-SIGRTMIN)/2));
}

CPPUNIT_TEST_SUITE_REGISTRATION(SignalTests);

#endif //USE_SIGNAL_HANDLER

