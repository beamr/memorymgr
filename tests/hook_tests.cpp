/*

The MIT License (MIT)

Copyright (c) 2013 ICVT Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#include <iostream>
#include "hook_tests.h"
#include "hooks.h"
#include "memmgr.h"
#include <malloc.h>

#include <unistd.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void HookTests::setUp() {
}

void HookTests::tearDown() {
}

void HookTests::TestMalloc() {
	int c1 = MemoryMgr::instance().allocsCount();
	void *buf = malloc(1000);
	assert(myfn_malloc);
	assert(buf);
	int c2 = MemoryMgr::instance().allocsCount();
	assert(c2 == c1 + 1);
	free(buf);
}

void HookTests::TestCalloc() {
	int c1 = MemoryMgr::instance().allocsCount();
	void *buf = calloc(1000, 10);
	assert(myfn_calloc);
	assert(buf);
	int c2 = MemoryMgr::instance().allocsCount();
	assert(c2 == c1 + 1);
	free(buf);
}

void HookTests::TestFree() {
	assert(myfn_free);
	int c1 = MemoryMgr::instance().allocsCount();
	void *buf = malloc(1000);
	assert(buf);
	int c2 = MemoryMgr::instance().allocsCount();
	assert(c2 == c1 + 1);
	free(buf);
	int c3 = MemoryMgr::instance().allocsCount();
	assert(c3 == c1);
}

void HookTests::TestRealloc() {
	int c1 = MemoryMgr::instance().allocsCount();
	void *buf = malloc(1000);
	assert(buf);
	int c2 = MemoryMgr::instance().allocsCount();
	assert(c2 == c1 + 1);
	void *buf2 = realloc(buf, 2000);
	assert(myfn_realloc);
	assert(buf2);
	int c3 = MemoryMgr::instance().allocsCount();
	assert(c3 == c2);
	free(buf2);
}

void HookTests::TestMemalign() {
	int c1 = MemoryMgr::instance().allocsCount();
	void *buf = memalign(32, 1000);
	assert(myfn_memalign);
	assert(buf);
	int c2 = MemoryMgr::instance().allocsCount();
	assert(c2 == c1 + 1);
	free(buf);
}

void HookTests::TestPosixMemalign() {
	int c1 = MemoryMgr::instance().allocsCount();
	void* buf = NULL;
	assert(posix_memalign(&buf, 32, 1000) == 0);
	assert(myfn_posix_memalign);
	assert(buf);
	int c2 = MemoryMgr::instance().allocsCount();
	assert(c2 == c1 + 1);
	free(buf);
	int c3 = MemoryMgr::instance().allocsCount();
	assert(c3 == c1);

}

void process_mem_usage(double& vm_usage, double& resident_set)
{
   using std::ios_base;
   using std::ifstream;
   using std::string;

   vm_usage     = 0.0;
   resident_set = 0.0;

   // 'file' stat seems to give the most reliable results
   //
   ifstream stat_stream("/proc/self/stat",ios_base::in);

   // dummy vars for leading entries in stat that we don't care about
   //
   string pid, comm, state, ppid, pgrp, session, tty_nr;
   string tpgid, flags, minflt, cminflt, majflt, cmajflt;
   string utime, stime, cutime, cstime, priority, nice;
   string O, itrealvalue, starttime;

   // the two fields we want
   //
   unsigned long vsize;
   long rss;

   stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
               >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
               >> utime >> stime >> cutime >> cstime >> priority >> nice
               >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

   stat_stream.close();

   long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
   vm_usage     = vsize / 1024.0;
   resident_set = rss * page_size_kb;
}


void HookTests::TestMemoryLeaks1() {
	MemoryMgr::instance().releaseAllSnapshots();
	void* p = malloc(1000);
	MemoryMgr::instance().takeSnapshot(2);   //p lives in 2
	assert(MemoryMgr::instance().isAlloced(p));
	assert(MemoryMgr::instance().isLive(p));
	free(p);
	assert(!MemoryMgr::instance().isLive(p));
	assert(MemoryMgr::instance().isAlloced(p));
	MemoryMgr::instance().restoreSnapshot(2);
	assert(MemoryMgr::instance().isLive(p));
	assert(MemoryMgr::instance().isAlloced(p));
	free(p);
	assert(!MemoryMgr::instance().isLive(p));
	MemoryMgr::instance().releaseSnapshot(2);
	assert(!MemoryMgr::instance().isAlloced(p));
}

void HookTests::TestMemoryLeaks2() {
	MemoryMgr::instance().releaseAllSnapshots();
	void* p = malloc(1000);
	assert(MemoryMgr::instance().isAlloced(p));
	MemoryMgr::instance().takeSnapshot(2);   //p lives in 2
	MemoryMgr::instance().takeSnapshot(3);   //p lives in 2 and 3
	assert(MemoryMgr::instance().isAlloced(p));
	assert(MemoryMgr::instance().isLive(p));
	free(p);
	assert(MemoryMgr::instance().isAlloced(p));
	assert(!MemoryMgr::instance().isLive(p));
	MemoryMgr::instance().takeSnapshot(4);   //p lives in 2 and 3
	assert(MemoryMgr::instance().isAlloced(p));
	MemoryMgr::instance().takeSnapshot(3); //p is removed from old 3, and remains in 2
	assert(MemoryMgr::instance().isAlloced(p));
	MemoryMgr::instance().takeSnapshot(2); // p is removed from old 2, and killed
	assert(!MemoryMgr::instance().isAlloced(p));
	MemoryMgr::instance().takeSnapshot(2);
	assert(!MemoryMgr::instance().isAlloced(p));
	MemoryMgr::instance().releaseSnapshot(4);
	assert(!MemoryMgr::instance().isAlloced(p));
	MemoryMgr::instance().releaseSnapshot(2);
	assert(!MemoryMgr::instance().isAlloced(p));
	MemoryMgr::instance().releaseSnapshot(3);
	assert(!MemoryMgr::instance().isAlloced(p));
}

CPPUNIT_TEST_SUITE_REGISTRATION(HookTests);
