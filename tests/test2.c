/*

The MIT License (MIT)

Copyright (c) 2013 ICVT Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#include <stdio.h>
#include <stdlib.h>
#include  <signal.h>
#include <unistd.h>

void signal1(int id) {
	kill(getpid(),id);
}

int main( int argc, char **argv ) {
	char *p[10];
	int s,i;
	for(i=0; i<10; i++) {
		p[i]= malloc(i*1000);
		for(s=0; s<i*1000; s++)
			p[i][s]= (i*100);
	}

	printf("before:\n");
	for(i=0; i<10; i++)
		printf("#%d %d\n", i, p[i][0]);

	signal1(SIGRTMIN);

	printf("afetr:\n"); 
	for(i=0; i<10; i++)
		p[i]= memset(p[i], i, i*1000);
	for(i=0; i<10; i++)
		printf("#%d %d\n", i, p[i][0]);
	signal1(SIGRTMIN+2);

	signal1(SIGRTMIN+1);
	printf("back to #1\n");
	for(i=0; i<10; i++)
		printf("#%d %d\n", i, p[i][0]);
	printf("back to #2\n"); 
	signal1(SIGRTMIN+3);

	for(i=0; i<10; i++)
		printf("#%d %d\n", i, p[i][0]);
}