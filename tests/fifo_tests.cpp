/*

The MIT License (MIT)

Copyright (c) 2013 ICVT Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#ifdef USE_FIFO_HANDLER

#include <thread>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include "fifo_handler.h"
#include "memmgr.h"
#include "fifo_tests.h"


using namespace std;

FIFOTests::FIFOTests() {
	long pid = (long)getpid();

	stringstream i, o;
	const char* prefix = getenv("MMGR_FIFO_PATH") ? getenv("MMGR_FIFO_PATH") : "/tmp";
	i << prefix << "/memmgr." << pid << ".in" << ends;
	o << prefix << "/memmgr." << pid << ".out" << ends;
	_reader_name = o.str();	//use opposite of what mmgr uses, i.e. read from out 
	_writer_name = i.str();	//use opposite of what mmgr uses i.e. write to in

	  struct stat buffer;
	  while(stat (_reader_name.c_str(), &buffer) != 0);
	  while(stat (_writer_name.c_str(), &buffer) != 0);
	thread t1(FIFOOpener<ifstream>(_reader, _reader_name, ios::in));
	thread t2(FIFOOpener<ofstream>(_writer, _writer_name, ios::out));
	t1.join();
	t2.join();
}

void FIFOTests::setUp() {
}

void FIFOTests::tearDown() {
}

void FIFOTests::sendCmd(const char* s) {
	char buf[256];

	_writer << s << endl;
	_writer.flush();
	_reader.getline(buf, 256);
	CPPUNIT_ASSERT(strcmp("done", buf) == 0);
}

void FIFOTests::Test() {
	sendCmd("reset");
	CPPUNIT_ASSERT(MemoryMgr::instance().snapshotCount() == 0);

	sendCmd("snapshot 10");
	sendCmd("dump /tmp/mmgr.1.txt");
	CPPUNIT_ASSERT(MemoryMgr::instance().snapshotCount() == 1);

	sendCmd("snapshot 17");
	sendCmd("dump /tmp/mmgr.2.txt");
	CPPUNIT_ASSERT(MemoryMgr::instance().snapshotCount() == 2);

	sendCmd("snapshot 10");
	sendCmd("dump /tmp/mmgr.3.txt");
	CPPUNIT_ASSERT(MemoryMgr::instance().snapshotCount() == 2);

	sendCmd("restore 17");
	sendCmd("dump /tmp/mmgr.4.txt");
	sendCmd("reset");
	sendCmd("dump /tmp/mmgr.5.txt");
	CPPUNIT_ASSERT(MemoryMgr::instance().snapshotCount() == 0);

	_writer << "exit" << endl;
	_writer.flush();
}

CPPUNIT_TEST_SUITE_REGISTRATION(FIFOTests);

#endif //USE_FIFO_HANDLER

