/*

This is a sample how to use mmgr without preloading
It assumes mmgr was built with DISABLE_OVERRIDE
g++ -g sample_using_wraps.cpp -I../include -L. -lmmgr -Wl,--wrap=malloc -Wl,--wrap=free

*/

#include <stddef.h>
#include <mmgr.h>
#include <malloc.h>

extern "C" {

void* __wrap_malloc(size_t s) {
	printf("called with %d\n", int(s));
	return mmgr_malloc(s);
}

void __wrap_free(void* ptr) {
	printf("releasing\n");
	mmgr_free(ptr);
}

}

int main() {
  void *p = malloc(100);
  free(p);
}
