/*

The MIT License (MIT)

Copyright (c) 2013 ICVT Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#ifndef _FIFO_TESTS_H
#define _FIFO_TESTS_H

#include <cppunit/extensions/HelperMacros.h>
#include <string>
#include <fstream>
#include <fstream>


class FIFOTests : public CppUnit::TestFixture {
public:
	FIFOTests();

	void setUp();
	void tearDown();
	void Test();

	void sendCmd(const char* s);

	CPPUNIT_TEST_SUITE(FIFOTests);
	CPPUNIT_TEST(Test);
	CPPUNIT_TEST_SUITE_END();

private:
	std::string _reader_name;
	std::string _writer_name;
	std::ifstream _reader;
	std::ofstream _writer;
};

#endif //_FIFO_TESTS_H

