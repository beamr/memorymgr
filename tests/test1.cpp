/*

The MIT License (MIT)

Copyright (c) 2013 ICVT Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#include <iostream>
#include <stdlib.h>
#include <cassert>
#include <cstring>
#include <random>

using namespace std;

int main() {
	cout << "Hello world!" << endl;
	char* buf0 = (char*)malloc(110000);
	free(buf0);

	cout << "Allocating two buffers" << endl;
	char* buf1 = (char*)malloc(110000);
	char* buf2 = (char*)malloc(110000);

	assert(buf1);
	assert(buf2);

	cout << "Initializing buffers" << endl;
	memset(buf1, 7, 110000);
	memset(buf2, 7, 110000);

	cout << "Comparing buffers" << endl;
	assert(memcmp(buf1, buf2, 110000) == 0);

	cout << "take your snapshot now, and then press ENTER, ENTER, ENTER to continue" << endl;
	getchar();
	getchar();
	getchar();

	cout << "changing buffer 2" << endl;
	memset(buf2 + rand()%110000, 0, 1);

	cout << "Comparing buffers" << endl;
	assert(memcmp(buf1, buf2, 110000) != 0);

	cout << "restore the snapshot now, and then press ENTER, ENTER, ENTER to continue" << endl;
	getchar();
	getchar();
	getchar();

	cout << "Comparing buffers" << endl;
	assert(memcmp(buf1, buf2, 110000) == 0);
	free(buf0);
	free(buf1);

	cout << "press ENTER, ENTER, ENTER to continue" << endl;
	getchar();
	getchar();
	getchar();

	cout << "done." << endl;

	return 0;
}
