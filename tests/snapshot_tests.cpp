/*

The MIT License (MIT)

Copyright (c) 2013 ICVT Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#include <iostream>
#include <sys/types.h>
#include <snapshot.h>
#include <unistd.h>
#include "snapshot_tests.h"
#include "memmgr.h"
#include <malloc.h>
#include <cstring>
#include <algorithm>

using namespace std;

extern int g_last_signum_handled;

void SnapshotTests::setUp() {
}

void SnapshotTests::tearDown() {
}

void SnapshotTests::TestRestoreDoesNotDeleteSnaps() {
	////cerr << "TestRestoreDoesNotDeleteSnaps" << endl;
	MemoryMgr::instance().releaseAllSnapshots();
	CPPUNIT_ASSERT(MemoryMgr::instance().snapshotCount() == 0);
	MemoryMgr::instance().takeSnapshot(0);
	CPPUNIT_ASSERT(MemoryMgr::instance().snapshotCount() == 1);
	MemoryMgr::instance().takeSnapshot(1);
	CPPUNIT_ASSERT(MemoryMgr::instance().snapshotCount() == 2);
	MemoryMgr::instance().restoreSnapshot(0);
	CPPUNIT_ASSERT(MemoryMgr::instance().snapshotCount() == 2);
	MemoryMgr::instance().restoreSnapshot(1);
	CPPUNIT_ASSERT(MemoryMgr::instance().snapshotCount() == 2);
	MemoryMgr::instance().releaseAllSnapshots();
	CPPUNIT_ASSERT(MemoryMgr::instance().snapshotCount() == 0);
}

void SnapshotTests::testSnapRestore(char* ptr, size_t size, int snapIndex) {
	for(unsigned int i=0; i<size; ++i) {
		ptr[i] = i%10;
	}
	MemoryMgr::instance().takeSnapshot(snapIndex);
	memset(ptr, 0, size);
	for(unsigned int i=0; i<size; ++i) {
		CPPUNIT_ASSERT(ptr[i] == 0);
	}
	MemoryMgr::instance().restoreSnapshot(snapIndex);
	for(unsigned int i=0; i<size; ++i) {
		CPPUNIT_ASSERT(ptr[i] == (i%10));
	}
}

void SnapshotTests::TestPosixMemalign() {
	MemoryMgr::instance().releaseAllSnapshots();
	char *buf = NULL;
	char **pbuf = &buf;
	posix_memalign((void**)pbuf, 8, 1000);
	testSnapRestore(buf, 1000, 2);
	free(buf);
}

void SnapshotTests::TestMemalign() {
	MemoryMgr::instance().releaseAllSnapshots();
	char *buf = (char*)memalign(8, 1000);
	testSnapRestore(buf, 1000, 3);
	free(buf);
}

void SnapshotTests::TestCalloc() {
	MemoryMgr::instance().releaseAllSnapshots();
	char *buf = (char*)calloc(8, 22);
	testSnapRestore(buf, 22, 9);
	free(buf);
}

void SnapshotTests::TestMalloc() {
	MemoryMgr::instance().releaseAllSnapshots();
	char *buf = (char*)malloc(557);
	testSnapRestore(buf, 557, 17);
	free(buf);
}

void SnapshotTests::TestRealloc() {
	MemoryMgr::instance().releaseAllSnapshots();
	char *buf = (char*)malloc(557);
	testSnapRestore(buf, 557, 17);
	buf = (char*)realloc(buf, 250);
	testSnapRestore(buf, 250, 19);
	buf = (char*)realloc(buf, 600);
	testSnapRestore(buf, 600, 14);
	free(buf);
}

void SnapshotTests::Test2442() {
	MemoryMgr::instance().releaseAllSnapshots();
	char *buf = (char*)malloc(10);
	for(int i=0; i<10; ++i)
		buf[i] = i;

	MemoryMgr::instance().takeSnapshot(2);
	memset(buf, 0, 10);
	for(int i=0; i<10; ++i)
		CPPUNIT_ASSERT(buf[i] == 0);

	free(buf);

	MemoryMgr::instance().restoreSnapshot(2);
	MemoryMgr::instance().takeSnapshot(4);
	for(int i=0; i<10; ++i)
		CPPUNIT_ASSERT(buf[i] == i);

	memset(buf, 7, 10);
	for(int i=0; i<10; ++i)
		CPPUNIT_ASSERT(buf[i] == 7);

	free(buf);

	MemoryMgr::instance().restoreSnapshot(4);
	MemoryMgr::instance().takeSnapshot(2);
	for(int i=0; i<10; ++i)
		CPPUNIT_ASSERT(buf[i] == i);

	MemoryMgr::instance().dump("/tmp/mmgr.txt");

	free(buf);
}

void SnapshotTests::TestLoopWithLocal() {
	int* volatile p = (int*)malloc(sizeof(int));
	*p = 1;
	MemoryMgr::instance().takeSnapshot(1);
	//cerr << endl;
	for(int i=0; i<10; ++i) {
		TestLoopWithLocalHelper(p);
		CPPUNIT_ASSERT(*p == 2);
	}
	free(p);
}

int SnapshotTests::TestLoopWithLocalHelper(int* p) {
	MemoryMgr::instance().restoreSnapshot(1);
	int x = *p;
	//CPPUNIT_ASSERT(x == 1);

	// Restoring to this point breaks the test
	// because the value of *p is already placed into the local variable x
	// and x is not rollbacked
	// MemoryMgr::instance().restoreSnapshot(1);
	x *= 2;
	*p = x;
	return *p;
}

void SnapshotTests::TestUpdate() {
	//cerr << "Starting update test" << endl;
	MemoryMgr::instance().releaseAllSnapshots();

	int* p = (int*)malloc(sizeof(int));
	CPPUNIT_ASSERT(MemoryMgr::instance().isTracked(p));
	//cerr << "p: " << p << endl;

	*p = 5;
	//cerr << "Taking snapshot 1" << endl;
	MemoryMgr::instance().takeSnapshot(1);

	int* q = (int*)malloc(sizeof(int));
	CPPUNIT_ASSERT(MemoryMgr::instance().isTracked(q));
	//cerr << "q: " << q << endl;

	*q = 6;
	//cerr << "*p: " << *p << endl;
	//cerr << "*q: " << *q << endl;

	//cerr << "Taking snapshot 1" << endl;
	MemoryMgr::instance().takeSnapshot(1);
	CPPUNIT_ASSERT(MemoryMgr::instance().isTracked(q));
	CPPUNIT_ASSERT(MemoryMgr::instance().countSnapshots(q) == 1);
	//cerr << "*p: " << *p << endl;
	//cerr << "*q: " << *q << endl;

	//cerr << "Releasing p" << endl;
	*p = 0;
	free(p);

	//cerr << "Releasing q" << endl;
	*q = 0;
	free(q);

	//cerr << "*p: " << *p << endl;
	//cerr << "*q: " << *q << endl;
	//cerr << "Restoring snapshot" << endl;
	CPPUNIT_ASSERT(MemoryMgr::instance().isTracked(q));
	MemoryMgr::instance().restoreSnapshot(1);
	CPPUNIT_ASSERT(MemoryMgr::instance().isTracked(q));
	//cerr << "*p: " << *p << endl;
	//cerr << "*q: " << *q << endl;

	CPPUNIT_ASSERT(*q == 6);
	CPPUNIT_ASSERT(*p == 5);
	

	MemoryMgr::instance().releaseAllSnapshots();
	free(p);
	free(q);
}

uint16_t gen_crc16(const uint8_t *data, uint16_t size);
void SnapshotTests::TestLotsOfIterations() {
    //setup some random buffers
    //take a checksum
    //take a snapshot
    //modify the buffers
    //restore
    //go back

    int *buffers[100];
    size_t buffer_sizes[100];
    uint16_t crc[100];
    const int rounds = 10;
    memset(buffer_sizes, 0, sizeof(size_t)*100);
    
    for (int i=0; i<rounds; ++i) {
        cout << "Statring iteration " << i << endl;

        int snap = rand() % 3;
        MemoryMgr::instance().takeSnapshot(snap); //this snapshot is not used. it will be overwritten

        //allocate some buffers, and fill with random data;
        const int nBuffers = rand() % 100;
        cout << "nBuffers: " << nBuffers << endl;
        for (int j=0; j<nBuffers; ++j) {
            if (buffer_sizes[j] == 0) {
                buffer_sizes[j] = 1024 + rand() % (1024 * 999);  //1KB to 10KB
                buffers[j] = (int*)malloc(buffer_sizes[j] * sizeof(int));
            }

            memset(buffers[j], 0, buffer_sizes[j] * sizeof(int));
            for (int k=0; k<std::min((size_t)100, buffer_sizes[j]); ++k) {
                int val = rand() + 1;
                buffers[j][rand() % buffer_sizes[j]] = val;
            }
        }

        //update snapshot
        MemoryMgr::instance().takeSnapshot(snap); 

        //get a checksum of current state
        for (int j=0; j<nBuffers; ++j) {
            crc[j] = gen_crc16((uint8_t*)buffers[j], buffer_sizes[j]*sizeof(int));
        }

        //screw up data
        for (int j=0; j<nBuffers; ++j) {
            for (int k=0; k<std::min((size_t)10, buffer_sizes[j]); ++k) {
                int val = rand();
                buffers[j][rand() % buffer_sizes[j]] = val;
            }
        }

        //free half of the buffers
        //the should be "restored" when the snapshot is restored
        //note that we don't zero the buffer_size because we need it when the 
        //buffer is later restored (that memory is not on the heap)
        for (int j=0; j<nBuffers; ++j) {
            if (rand() % 2) {
                free(buffers[j]); 
            } else {
                // do nothing;
            }
        }

        MemoryMgr::instance().restoreSnapshot(snap);

        //validate restore
        for (int j=0; j<nBuffers; ++j) {
            CPPUNIT_ASSERT(crc[j] == gen_crc16((uint8_t*)buffers[j], buffer_sizes[j]*sizeof(int)));
        }

        //free some of the buffers, and let others be carried over to next iteration
        for (int j=0; j<nBuffers; ++j) {
            if (rand() % 10) {
                free(buffers[j]); 
                buffer_sizes[j] = 0;
            } else {
                // do nothing;
            }
        }
    }
}

CPPUNIT_TEST_SUITE_REGISTRATION(SnapshotTests);
